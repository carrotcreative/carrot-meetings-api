module.exports =

  base:
    google_client_id: '813719990437-2cue6ovsrtlgmsamce599l7j79ogctlt.apps.googleusercontent.com'
    google_client_secret: 'v0eQ331HbhkRcQdKpmUdmhLV'
    google_calendar_id: 'carrotmeetingstest@gmail.com'
    google_refresh_token: '1/1lLweuON32568EvXG61gumWUlSsUn5BMqW3p4UXrka0'
    carrot_api_signature: '/Ez+gPa6wLGG/6PB0cCx7weA7ZXHFKpGdphB01bhUus='
    location: 'Carrot Creative, 45 Main St., Suite 1100, Brooklyn, NY 11201'

  production:
    google_calendar_id: 'meetings@carrotcreative.com'
    google_refresh_token: '1/5K5rDcWLOSCC7HaUjTy3s1PvuPAu8C7UxicuEdegr_I'
    database:
      client: 'pg'
      connection: process.env.DATABASE_URL

  development:
    database:
      client: 'pg'
      connection: 'postgresql://localhost/carrot_meetings_development'

  test:
    database:
      client: 'pg'
      connection: 'postgresql://localhost/carrot_meetings_test'

  travis:
    database:
      client: 'pg'
      connection: 'postgresql://postgres@localhost/carrot_meetings_test'
