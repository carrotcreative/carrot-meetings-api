Carrot Meetings
===============

[![Build Status](https://magnum.travis-ci.com/carrot/carrot-meetings.svg?token=DPL2U74nnKpRi3RZcFDx&branch=master)](https://magnum.travis-ci.com/carrot/carrot-meetings)

An API for managing meeting rooms at Carrot.

### Requirements

- Node.js + npm
- MySQL
- Heroku (for deployment)

### Setup

1. Clone the repository:

  ```sh
  $ git clone git@github.com:carrot/carrot-meetings
  $ cd carrot-meetings
  ```

2. Install the package dependencies:

  ```sh
  $ npm install
  ```

3. Migrate and seed the database (see `config.coffee` for database configuration):

  ```sh
  $ npm run db:migrate
  $ npm run db:seed
  ```

### Usage

```sh
$ npm start
```

### Documentation

- **Endpoints**

  - [Meetings](docs/endpoints/meetings.md)
  - [Rooms](docs/endpoints/rooms.md)

### Authentication

This API uses a very basic token-based authentication. This type of auth is only secure if the front-end is behind a truly authenticated wall. For example, the primary implementation of this app will be behind google apps auth, so an attacker would have to guess the token or be authenticated through carrot google apps and view the source in order to gain access. That means that this API should not be utilized anywhere that the front end is open to the public, as anyone would be able to simply view source to gain full access to the API, as you need to send the token with any request, which means it would appear in the source.

The token can be found in [token](token), and if it is compromised for any reason, can be regenerated using the command `npm run token:generate`. This command will remove the old token and replace it with a new, so any sites that use it will be broken and get 401s back from any request until they are updated with the new token.

In order to use the token, it should be set as the value of a X-Carrot-Auth header in each request.

### Deploying

Hosted on Heroku at [carrot-meetings.herokuapp.com](http://carrot-meetings.herokuapp.com/). Permission to deploy must be granted via the **devops@carrotcreative.com** Heroku account.

### Contributing

Details on running tests and contributing [can be found here](CONTRIBUTING.md).
