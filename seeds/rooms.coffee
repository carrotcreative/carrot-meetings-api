exports.seed = (knex, promise) ->
  table = knex 'rooms'
  table
    .truncate()
    .then ->
      table.insert [
        { name: 'Biggie Smalls' },
        { name: 'Midler' },
        { name: 'Stadium' },
        { name: 'Owl Shop' },
        { name: 'Banana Stand' },
        { name: 'Green Monster' },
        { name: 'Kitchen' },
        { name: 'Orange Couches' }
      ]
