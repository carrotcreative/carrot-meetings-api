#### `GET /rooms`

Return an array of all rooms.

**Example response:**

```json
[
  {
    "id": 1,
    "name": "Biggie Smalls"
  }, {
    "id": 2,
    "name": "Midler"
  }
]
```

#### `GET /rooms/:id`

Return a room by `:id`.

#### `PATCH /rooms/:id`

Update a room by `:id`.

**Expected parameters:**

Name                  | Type        | Required? | Description
----------------------|-------------|-----------|----------------------------------
name                  | String      | Y         | name of the room

#### `GET /rooms/:id/meetings`

Return an array of all meetings in room by `:id`.

#### `GET /rooms/:id/meetings/today`

Return an array of all meetings in room by `:id` and scheduled for the current date.

#### `GET /rooms/:id/meetings/pending`

Return an array of all meetings in room by `:id` and marked as `pending`.

#### `GET /rooms/:id/meetings/approved`

Return an array of all meetings in room by `:id` and marked as `approved`.

#### `GET /rooms/:id/meetings/on/:date`

Return an array of all meetings in room by `:id` and scheduled for `:date`.

#### `GET /meetings/between/:start_date/:end_date`

Return an array of all meetings in room by `:id` and scheduled between `:start_date` and `:end_date`.
