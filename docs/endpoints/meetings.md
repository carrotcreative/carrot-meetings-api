#### `GET /meetings`

Return an array of all meetings.

**Example response:**

```json
[
  {
    "id": 1,
    "room_id": 1,
    "name": "Production Meeting",
    "start_time": "2014-12-09T22:30:00.000Z",
    "end_time": "2014-12-09T23:00:00.000Z",
    "author_carrot_slug": "noahporteschaikin",
    "notes": null,
    "state": "approved",
    "room": {
      "id": 1,
      "name": "Biggie Smalls"
    }
  }
]
```

#### `POST /meetings`

Create a meeting.

**Expected parameters:**

Name                  | Type        | Required? | Description
----------------------|-------------|-----------|----------------------------------
name                  | String      | Y         | name of the meeting
author_carrot_slug    | String      | Y         | Carrot slug of the meeting author
room_id               | Integer     | N         | `id` of the meeting room
start_time            | DateTime    | N         | time meeting starts
end_time              | DateTime    | N         | time meeting ends
notes                 | String      | N         | anything (requests, etc.)

#### `GET /meetings/pending`

Return an array of all meetings marked as `pending`.

#### `GET /meetings/approved`

Return an array of all meetings marked as `pending`.

#### `GET /meetings/today`

Return an array of all meetings scheduled for the current date.

#### `GET /meetings/on/:date`

Return an array of all meetings scheduled for `:date`.

#### `GET /meetings/between/:start_date/:end_date`

Return an array of all meetings scheduled between `:start_date` and `:end_date`.

#### `GET /meetings/:id`

Return a meeting by `:id`.

#### `PATCH /meetings/:id`

Update a meeting by `:id`.

#### `DELETE /meetings/:id`

Delete a meeting by `:id`.

#### `PATCH /meetings/:id/approve`

Mark a meeting by `:id` as `approved`.

**Expected parameters:**

Name                  | Type        | Required? | Description
----------------------|-------------|-----------|----------------------------------
room_id               | Integer     | Y         | `id` of the meeting room
