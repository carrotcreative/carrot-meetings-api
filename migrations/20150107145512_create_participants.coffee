exports.up = (knex, promise) ->
  knex.schema.createTable 'participants', (table) ->
    table.increments()
      .primary()
    table.integer 'meeting_id'
    table.string 'carrot_slug'
    table.string 'name'
    table.string 'email'
    table.string 'title'
    table.string 'department'
    table.string 'avatar'
    table.string 'office'

exports.down = (knex, promise) ->
  knex.schema.dropTable 'participants'
