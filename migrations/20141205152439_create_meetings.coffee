exports.up = (knex, promise) ->
  knex.schema.createTable 'meetings', (table) ->
    table.increments()
      .primary()
    table.integer 'room_id'
    table.string 'name'
    table.dateTime 'start_time'
    table.dateTime 'end_time'
    table.string 'author_carrot_slug'
    table.text 'notes'
    table.string 'event_id'
    table.string 'state'
      .defaultTo 'pending'

exports.down = (knex, promise) ->
  knex.schema.dropTable 'meetings'
