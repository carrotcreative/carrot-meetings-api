exports.up = (knex, promise) ->
  knex.schema.createTable 'invites', (table) ->
    table.increments()
      .primary()
    table.integer 'meeting_id'
    table.string 'email'

exports.down = (knex, promise) ->
  knex.schema.dropTable 'invites'
