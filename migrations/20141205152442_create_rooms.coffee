exports.up = (knex, promise) ->
  createTable = knex.schema.createTable 'rooms', (table) ->
    table.increments()
      .primary()
    table.string 'name'

exports.down = (knex, promise) ->
  knex.schema.dropTable 'rooms'
