process.env.NODE_ENV = if process.env.TRAVIS then 'travis' else 'test'

chai          = require 'chai'
http          = require 'http'
_             = require 'lodash'
Promise       = require 'bluebird'

app           = require '../lib'
db            = require '../lib/database'
Meeting       = require '../lib/models/meeting'
Participant   = require '../lib/models/participant'
Invite        = require '../lib/models/invite'
Room          = require '../lib/models/room'

should  = chai.should()

options =
  host: 'localhost'
  port: 4567

before (done) ->
  db
    .connection
    .migrate
    .latest()
    .then -> db.connection.seed.run()
    .then -> done()

afterEach (done) ->
  db
    .connection
    .schema
    .raw 'TRUNCATE TABLE invites'
    .raw 'TRUNCATE TABLE meetings'
    .raw 'TRUNCATE TABLE participants'
    .then -> done()

describe 'server', ->

  server = null

  before (done) ->
    server = app.listen options.port, ->
      done()

  after (done) ->
    server.close()
    done()

  it 'should exist', (done) ->
    should.exist app
    done()

  it "should be listening at localhost:#{options.port}", (done) ->
    http.get host: options.host, port: options.port, method: 'GET', (res) ->
      res.statusCode.should.eql 401
      done()

describe 'models', ->

  describe 'Meeting', ->

    it 'should create a meeting', (done) ->
      new Meeting(
          name: 'Production Meeting'
          start_time: new Date '2015-12-31 13:00:00'
          end_time: new Date '2015-12-31 14:00:00'
          author_carrot_slug: 'kyle'
        )
        .save()
        .then (meeting) -> done()

    describe 'scopes', ->

      it 'should return meeting between specified dates', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            room_id: 1
          )
          .save()
          .then ->
            new Meeting()
              .between_dates(new Date('2015-12-30'), new Date('2016-01-01'))
              .fetchAll()
          .then (collection) ->
            collection.length.should.eq 1
            done()

      it 'should return meeting on specified date', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            room_id: 1
          )
          .save()
          .then ->
            new Meeting()
              .is_on_date(new Date('2015-12-31 13:00:00'))
              .fetchAll()
          .then (collection) ->
            collection.length.should.eq 1
            done()

      it 'should return approved meetings', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            state: 'approved'
            room_id: 1
          )
          .save()
          .then ->
            new Meeting()
              .where_is_approved()
              .fetchAll()
          .then (collection) ->
            collection.length.should.eq 1
            done()

      it 'should return pending meetings', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            state: 'pending'
            room_id: 1
          )
          .save()
          .then ->
            new Meeting()
              .where_is_pending()
              .fetchAll()
          .then (collection) ->
            collection.length.should.eq 1
            done()

    describe 'instance methods', ->

      it 'is_pending() should return true if meeting is pending', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            state: 'pending'
            room_id: 1
          )
          .save()
          .then (meeting) ->
            meeting.is_pending().should.be.true()
            done()

      it 'is_approved() should return true if meeting is approved', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            state: 'approved'
            room_id: 1
          )
          .save()
          .then (meeting) ->
            meeting.is_approved().should.be.true()
            done()

      it 'is_pending() should return false if meeting is approved', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            state: 'approved'
            room_id: 1
          )
          .save()
          .then (meeting) ->
            meeting.is_pending().should.be.false()
            done()

      it 'is_approved() should return false if meeting is pending', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            state: 'pending'
            room_id: 1
          )
          .save()
          .then (meeting) ->
            meeting.is_approved().should.be.false()
            done()

    describe 'associations', ->

      it 'should return a room', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            room_id: 1
          )
          .save()
          .then (meeting) -> meeting.room().fetch()
          .then (room) ->
            room.attributes.id.should.eq 1
            done()

      it 'should return participants', (done) ->
        __meeting = null
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
          )
          .save()
          .then (meeting) -> __meeting = meeting
          .then -> new Invite(email: 'noah@porteschaikin.com', meeting_id: __meeting.attributes.id).save()
          .then -> __meeting.invites().fetch()
          .then (collection) ->
            collection.length.should.eq 1
            done()

      it 'should return invites', (done) ->
        __meeting = null
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            room_id: 1
          )
          .save()
          .then (meeting) -> __meeting = meeting
          .then -> new Participant(carrot_slug: 'kyle', meeting_id: __meeting.attributes.id).save()
          .then -> __meeting.participants().fetch()
          .then (collection) ->
            collection.length.should.eq 1
            done()

      it 'should return an event', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            room_id: 1
          )
          .save()
          .then (meeting) ->
            meeting.event().is_new().should.be.true()
            done()

      describe 'nested attributes', ->

        it 'should create invites if invites hash included', (done) ->
          new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            invites: [
              { email: "noah@porteschaikin.com" }
              { email: "foo@bar.com" }
            ]
          )
          .save()
          .then (model) ->
            model.related('invites').length.should.eq(2)
            done()

        it 'should create participants if participants hash included', (done) ->
          new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            participants: [
              { carrot_slug: "tom" }
              { carrot_slug: "kyle" }
            ]
          )
          .save()
          .then (model) ->
            model.related('participants').length.should.eq(2)
            done()

        it 'should fail if invites are invalid', (done) ->
          new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            invites: [
              { email: "tom" }
            ]
          )
          .save()
          .catch (e) ->
            e[0].should.eq 'A valid e-mail address is required'
            done()

        it 'should fail if participants are invalid', (done) ->
          new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            participants: [
              { carrot_slug: "" }
            ]
          )
          .save()
          .catch (e) ->
            e.should.include 'The carrot_slug is required'
            done()

        it 'should destroy nested model if _destroy is passed', (done) ->
          new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            invites: [
              { email: "foo@bar.com" }
            ]
          )
          .save()
          .then (model) ->
            model.save(invites: [{
              id: model.related('invites').models[0].id
              _destroy: 1
            }])
          .then (model) ->
            model.related('invites').length.should.eq(0)
            done()

    describe 'validations', ->

      it 'should fail without a name', (done) ->
        new Meeting(
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
          )
          .save()
          .catch (e) ->
            e.should.include 'The name is required'
            done()

      it 'should fail without a start time', (done) ->
        new Meeting(
            name: 'Production Meeting'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
          )
          .save()
          .catch (e) ->
            e.should.include 'The start time is required'
            done()

      it 'should fail without an end time', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            author_carrot_slug: 'kyle'
          )
          .save()
          .catch (e) ->
            e.should.include 'The end time is required'
            done()

      it 'should fail without a slug', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
          )
          .save()
          .catch (e) ->
            e.should.include 'The author_carrot_slug is required'
            done()

      it 'should fail if meeting starts after it ends', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 14:00:00'
            end_time: new Date '2015-12-31 13:00:00'
            author_carrot_slug: 'kyle'
          )
          .save()
          .catch (e) ->
            e.should.include 'The start time must be before the end time'
            done()

      it 'should fail if meeting is approved but has no room', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            state: 'approved'
          )
          .save()
          .catch (e) ->
            e.should.include 'The room is required'
            done()

      it 'should fail if room already has meeting at this time', (done) ->
        new Meeting(
            name: 'Production Meeting'
            room_id: 1
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            state: 'approved'
          )
          .save()
          .then ->
            new Meeting(
                name: 'Production Meeting'
                room_id: 1
                start_time: new Date '2015-12-31 13:00:00'
                end_time: new Date '2015-12-31 14:00:00'
                author_carrot_slug: 'kyle'
                state: 'approved'
              )
              .save()
          .catch (e) ->
            e.toString().should.eq 'Biggie Smalls has a conflicting meeting: Production Meeting on December 31, 2015 at 1:00 pm'
            done()

  describe 'Event', ->

    it 'should add google event id to meeting if meeting approved', (done) ->
      new Meeting(
          name: 'Production Meeting'
          room_id: 1
          start_time: new Date '2015-12-31 13:00:00'
          end_time: new Date '2015-12-31 14:00:00'
          author_carrot_slug: 'kyle'
          state: 'approved'
        )
        .save()
        .then (meeting) ->
          meeting.attributes.event_id.should.not.be.null()
          done()

  describe 'Invite', ->

    it 'should create an invite', (done) ->
      new Meeting(
        name: 'Production Meeting'
        room_id: 1
        start_time: new Date '2015-12-31 13:00:00'
        end_time: new Date '2015-12-31 14:00:00'
        author_carrot_slug: 'kyle'
        state: 'approved'
      )
      .save()
      .then (meeting) ->
        new Invite(meeting_id: meeting.attributes.id, email: 'noah@porteschaikin.com')
          .save()
      .then done()

    describe 'associations', ->

      it 'should return a meeting', (done) ->
        new Meeting(
          name: 'Production Meeting'
          room_id: 1
          start_time: new Date '2015-12-31 13:00:00'
          end_time: new Date '2015-12-31 14:00:00'
          author_carrot_slug: 'kyle'
        )
        .save()
        .then (meeting) ->
          new Invite(meeting_id: meeting.attributes.id, email: 'noah@porteschaikin.com')
            .save()
        .then (invite) -> invite.meeting().fetch()
        .then (meeting) ->
          meeting.isNew().should.be.false()
          done()

    describe 'validations', ->

      it 'should fail without a meeting', (done) ->
        new Invite(
            email: 'noah@porteschaikin.com'
          )
          .save()
          .catch (e) ->
            e.should.include 'The meeting is required'
            done()

      it 'should fail without an email', (done) ->
        new Invite(
            meeting_id: 1
          )
          .save()
          .catch (e) ->
            e.should.include 'A valid e-mail address is required'
            done()

      it 'should fail if same e-mail is invited to another meeting at the same time', (done) ->
        new Meeting(
          name: 'Production Meeting'
          room_id: 1
          start_time: new Date '2015-12-31 13:00:00'
          end_time: new Date '2015-12-31 14:00:00'
          author_carrot_slug: 'kyle'
          state: 'approved'
        )
        .save()
        .then (meeting) ->
          new Invite(meeting_id: meeting.attributes.id, email: "noah@porteschaikin.com")
            .save()
        .then ->
          new Meeting(
            name: 'Production Meeting'
            room_id: 4
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            state: 'approved'
          ).save()
        .then (meeting) ->
          new Invite(meeting_id: meeting.attributes.id, email: "noah@porteschaikin.com")
            .save()
        .catch (e) ->
          e.should.include 'noah@porteschaikin.com has a conflicting meeting: Production Meeting on December 31, 2015 at 1:00 pm'
          done()

  describe 'Participant', ->

    it 'should create a participant', (done) ->
      new Meeting(
        name: 'Production Meeting'
        room_id: 1
        start_time: new Date '2015-12-31 13:00:00'
        end_time: new Date '2015-12-31 14:00:00'
        author_carrot_slug: 'kyle'
        state: 'approved'
      )
      .save()
      .then (meeting) ->
        new Participant(meeting_id: meeting.attributes.id, carrot_slug: 'jeff')
          .save()
      .then -> done()

    describe 'associations', ->

      it 'should return a meeting', (done) ->
        new Meeting(
          name: 'Production Meeting'
          room_id: 1
          start_time: new Date '2015-12-31 13:00:00'
          end_time: new Date '2015-12-31 14:00:00'
          author_carrot_slug: 'kyle'
        )
        .save()
        .then (meeting) ->
          new Participant(meeting_id: meeting.attributes.id, carrot_slug: 'jeff')
            .save()
        .then (participant) -> participant.meeting().fetch()
        .then (meeting) ->
          meeting.isNew().should.be.false()
          done()

      it 'should return an employee', (done) ->
        new Meeting(
          name: 'Production Meeting'
          room_id: 1
          start_time: new Date '2015-12-31 13:00:00'
          end_time: new Date '2015-12-31 14:00:00'
          author_carrot_slug: 'kyle'
        )
        .save()
        .then (meeting) ->
          new Participant(
              carrot_slug: 'jeff'
              meeting_id: meeting.attributes.id
            )
            .save()
        .then (participant) -> participant.employee().fetch()
        .then (employee) ->
          employee.attributes.name.should.eq 'Jeff Escalante'
          done()

    describe 'validations', ->

      it 'should fail without a meeting', (done) ->
        new Participant(
            carrot_slug: 'jeff'
          )
          .save()
          .catch (e) ->
            e.should.include 'The meeting is required'
            done()

      it 'should fail without a carrot slug', (done) ->
        new Participant(
            meeting_id: 1
          )
          .save()
          .catch (e) ->
            e.should.include 'The carrot_slug is required'
            done()

      it 'should fail if same carrot_slug is a participant in another meeting at the same time', (done) ->
        new Meeting(
          name: 'Production Meeting'
          room_id: 1
          start_time: new Date '2015-12-31 13:00:00'
          end_time: new Date '2015-12-31 14:00:00'
          author_carrot_slug: 'kyle'
          state: 'approved'
        )
        .save()
        .then (meeting) -> new Participant(meeting_id: meeting.attributes.id, carrot_slug: "jeff").save()
        .then ->
          new Meeting(
            name: 'Production Meeting'
            room_id: 4
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            state: 'approved'
          ).save()
        .then (meeting) -> new Participant(meeting_id: meeting.attributes.id, carrot_slug: 'jeff').save()
        .catch (e) ->
          e.should.include 'Jeff Escalante has a conflicting meeting: Production Meeting on December 31, 2015 at 1:00 pm'
          done()

  describe 'Employee', ->

    it 'should fetch the correct employee', (done) ->
      new Meeting(
        name: 'Production Meeting'
        room_id: 1
        start_time: new Date '2015-12-31 13:00:00'
        end_time: new Date '2015-12-31 14:00:00'
        author_carrot_slug: 'kyle'
        state: 'approved'
      )
      .save()
      .then (meeting) -> new Participant(meeting_id: meeting.attributes.id, carrot_slug: "kyle").save()
      .then (participant) -> participant.employee().fetch()
      .then (employee) ->
        employee.attributes.name.should.eq 'Kyle MacDonald'
        done()

  describe 'Room', ->

    it 'should create a room', (done) ->
      new Room(
        name: 'Doge'
      )
      .save()
      .then (room) -> room.destroy()
      .then -> done()

    describe 'validations', ->

      it 'should fail without a name', (done) ->
        new Room()
          .save()
          .catch (e) ->
            e.should.include 'The name is required'
            done()

    describe 'associations', ->

      it 'should return meetings', (done) ->
        new Meeting(
            name: 'Production Meeting'
            start_time: new Date '2015-12-31 13:00:00'
            end_time: new Date '2015-12-31 14:00:00'
            author_carrot_slug: 'kyle'
            room_id: 1
          )
          .save()
          .then -> new Room(id: 1).fetch()
          .then (room) -> room.meetings().fetch()
          .then (collection) ->
            collection.length.should.eq 1
            done()
