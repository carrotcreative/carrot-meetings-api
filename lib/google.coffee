config    = require './config'
google    = require 'googleapis'

client    = new google.auth.OAuth2(config.google_client_id, config.google_client_secret)
client.setCredentials
  refresh_token: config.google_refresh_token
  expiry_date: 1
  token_type: 'Bearer'

google.options auth: client

module.exports = google
