config      = require './config'
knex        = require 'knex'
bookshelf   = require 'bookshelf'
path        = require 'path'

connection = knex config.database
bookshelf = bookshelf connection

require('./bookshelf/cast')(bookshelf)
require('./bookshelf/validators')(bookshelf, path.join(__dirname, 'validators'))
require('./bookshelf/nested')(bookshelf)

bookshelf.plugin 'registry'
bookshelf.plugin 'virtuals'

module.exports.connection = connection
module.exports.bookshelf = bookshelf
