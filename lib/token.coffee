express   = require 'express'
fs        = require 'fs'
path      = require 'path'

module.exports = app = express()

token_path  = path.join __dirname, '..', 'token'
token       = fs.readFileSync token_path, 'utf8'

app.use (req, res, next) ->
  if req.headers['x-carrot-auth'] == token
    next()
  else
    res.status 401
    res.end 'Unauthorized'
