config        = require '../config'
request       = require 'request'
url           = require 'url'
Promise       = require 'bluebird'
_             = require 'lodash'

API_BASE_URL  = "http://api.bycarrot.com"

class Employee

  constructor: (@participant) ->
    @attributes ?= {}

  fetch: ->
    _get "staff/#{@participant.attributes.carrot_slug}", include_hidden: 1
      .then (response) => @attributes[key] = value for key, value of response[0]
      .then => @

_get = (path, parameters) ->
  new Promise (resolve, reject) ->
    request uri: _url(path), qs: _.merge(parameters, signature: config.carrot_api_signature), (error, response, body) ->
      if error then reject(error) else resolve JSON.parse(body).data

_url = (path) ->
  url.resolve API_BASE_URL, path

module.exports = Employee
