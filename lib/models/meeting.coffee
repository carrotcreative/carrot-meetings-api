require './room'
require './participant'
require './invite'
db              = require '../database'
Event           = require './event'
Promise         = require 'bluebird'
_               = require 'lodash'

STATES =
  APPROVED: 'approved'
  PENDING: 'pending'

Meeting = db.bookshelf.model 'Meeting',

  tableName: 'meetings'

  defaults:
    state: STATES.PENDING

  acceptsNestedAttributesFor: [
    'participants'
    'invites'
  ]

  cast:
    start_time: (start_time)  -> new Date start_time
    end_time:   (end_time)    -> new Date end_time

  initialize: ->
    @on 'saving', @validate_save
    @on 'saving', -> if @attributes.state is STATES.APPROVED then @event().save() else @event().destroy()
    @on 'destroying', -> @event().destroy()
    @on 'validating', -> @related('room').fetch()
    @validate 'presence', 'name'
    @validate 'presence', 'start_time'
      .msg 'The start time is required'
    @validate 'presence', 'end_time'
      .msg 'The end time is required'
    @validate 'presence', 'author_carrot_slug'
    @validate 'operator', 'start_time', '<=', 'end_time'
      .if (model) -> model.attributes.start_time && model.attributes.end_time
      .msg 'The start time must be before the end time'
    @validate 'presence', 'room_id'
      .msg 'The room is required'
      .if (model) -> model.attributes.state == STATES.APPROVED
    @validate 'conflicts',
        (model, builder) -> builder.where 'meetings.room_id', '=', model.attributes.room_id
        (model) -> model.relations.room.attributes.name
      .if (model) -> !!model.attributes.room_id

  room: ->
    @belongsTo 'Room'

  invites: ->
    @hasMany 'Invite'

  participants: ->
    @hasMany 'Participant'

  between_dates: (start_date, end_date) ->
    @query (builder) ->
      builder.whereRaw 'DATE(start_time) >= DATE(?) AND DATE(end_time) <= DATE(?)', [
        new Date(start_date)
        new Date(end_date)
      ]

  is_on_date: (date) ->
    @query (builder) ->
      builder.whereRaw 'DATE(start_time) = DATE(?)', [
        new Date(date)
      ]

  where_is_pending: ->
    @where state: STATES.PENDING

  where_is_approved: ->
    @where state: STATES.APPROVED

  approve: (attributes = {}) ->
    attributes.state = STATES.APPROVED
    @save attributes

  is_approved: ->
    @attributes.state is STATES.APPROVED

  is_pending: ->
    @attributes.state is STATES.PENDING

  event: ->
    @_event ?= new Event(@)

module.exports = Meeting
