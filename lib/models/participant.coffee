require './meeting'
Employee  = require './employee'
db        = require '../database'
Promise   = require 'bluebird'
_         = require 'lodash'

Participant = db.bookshelf.model 'Participant',

  tableName: 'participants'

  initialize: ->
    @on 'validating', @set_employee_attributes
    @on 'validating', -> @meeting().fetch()
    @validate 'presence', 'carrot_slug'
    @validate 'presence', 'meeting_id'
      .msg 'The meeting is required'
    @validate 'presence', 'name'
    @validate 'presence', 'email'
    @validate 'presence', 'title'
    @validate 'presence', 'avatar'
    @validate 'presence', 'office'
    @validate 'presence', 'department'
    @validate 'conflicts',
      (model, builder) ->
        builder
          .innerJoin 'participants', -> @on 'participants.meeting_id', '=', 'meetings.id'
          .where 'participants.carrot_slug', '=', model.attributes.carrot_slug
      (model) -> model.attributes.name

  meeting: ->
    @belongsTo 'Meeting'

  employee: ->
    @_employee ?= new Employee(@)

  set_employee_attributes: ->
    @employee()
      .fetch()
      .then (employee) =>
        @set 'name', employee.attributes.name
        @set 'email', employee.attributes.email
        @set 'title', employee.attributes.title
        @set 'avatar', employee.attributes.media_avatar
        @set 'office', employee.attributes.office
        @set 'department', employee.attributes.department

module.exports = Participant
