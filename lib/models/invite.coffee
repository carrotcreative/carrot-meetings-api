require './meeting'
db        = require '../database'

Invite = db.bookshelf.model 'Invite',

  tableName: 'invites'

  initialize: ->
    @validate 'presence', 'meeting_id'
      .msg 'The meeting is required'
    @validate 'match', 'email', 'email'
      .msg 'A valid e-mail address is required'
    @validate 'conflicts',
      (model, builder) ->
        builder
          .innerJoin 'invites', -> @on 'invites.meeting_id', '=', 'meetings.id'
          .where 'invites.email', '=', model.attributes.email
      (model) -> model.attributes.email

  meeting: ->
    @belongsTo 'Meeting'

module.exports = Invite
