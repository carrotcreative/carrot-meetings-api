require './meeting'
db        = require '../database'

Room = db.bookshelf.model 'Room',

  tableName: 'rooms'

  initialize: ->
    @validate 'presence', 'name'

  meetings: ->
    @hasMany 'Meeting'    

module.exports = Room
