google      = require '../google'
config      = require '../config'
_           = require 'lodash'
Promise     = require 'bluebird'
calendar    = google.calendar 'v3'

class Event

  constructor: (@meeting) ->
    @room         = meeting.related 'room'
    @participants = meeting.related 'participants'
    @invites      = meeting.related 'invites'

  save: ->
    _before @
      .then (event) -> if event.is_new() then _create event else _update event

  create: ->
    _before @
      .then (event) -> _create event if event.is_new()

  update: ->
    _before @
      .then (event) -> _update event unless event.is_new()

  destroy: ->
    _before @
      .then (event) -> _destroy event unless event.is_new()

  is_new: ->
    !(@meeting.attributes.event_id)

_before = (event) ->
  event.room.fetch()
    .then -> event.invites.fetch()
    .then -> event.participants.fetch()
    .then (collection) -> collection.models
    .then (participants) -> _.map participants, (participant) -> participant.employee().fetch()
    .then (array) -> Promise.all array
    .then -> event

_create = (event) ->
  parameters = _.pick _parameters(event), 'calendarId', 'resource'
  _request 'insert', parameters
    .then (response) -> event.meeting.set event_id: response.id

_update = (event) ->
  parameters = _.pick _parameters(event), 'calendarId', 'eventId', 'resource'
  _request 'update', parameters

_destroy = (event) ->
  parameters = _.pick _parameters(event), 'calendarId', 'eventId'
  _request 'delete', parameters
    .then -> event.meeting.set event_id: null

_parameters = (event) ->
  eventId: event.meeting.attributes.event_id
  calendarId: config.google_calendar_id
  resource:
    summary: "#{event.room.attributes.name} | #{event.meeting.attributes.name}"
    start: dateTime: event.meeting.attributes.start_time
    end: dateTime: event.meeting.attributes.end_time
    location: config.location
    attendees: _attendees(event)

_attendees = (event) ->
  invite_emails = _.map event.invites.models, (invite) -> email: invite.attributes.email
  participant_emails = _.map event.participants.models, (participant) -> email: participant.employee().attributes.email
  emails = invite_emails.concat participant_emails
  _.uniq emails

_request = (action, parameters) ->
  new Promise (resolve, reject) ->
    calendar.events[action] parameters, (error, response) ->
      if error then reject(error) else resolve(response)

module.exports = Event
