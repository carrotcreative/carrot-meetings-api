db      = require '../database'
moment  = require 'moment'

module.exports = (model) ->

  validate: (query_fn, name_fn) ->
    fetch_meetings model, query_fn
      .then (meeting) -> if meeting then throw "#{name_fn model} has a conflicting meeting: #{meeting_string meeting}"

fetch_meetings = (model, query_fn) ->
  new (db.bookshelf.model('Meeting'))()
    .query (builder) -> build_query builder, model, query_fn
    .fetch()

build_query = (builder, model, query_fn) ->
  builder
    .from 'meetings'
    .rightJoin 'meetings AS mtg', ->
      @on 'meetings.start_time', '<=', 'mtg.end_time'
        .andOn 'mtg.start_time', '<=', 'meetings.end_time'
    .where 'mtg.state', '=', 'approved'
  query_fn model, builder

meeting_string = (meeting) ->
  start_time = moment meeting.attributes.start_time
  "#{meeting.attributes.name} on #{start_time.format 'MMMM D, YYYY [at] h:mm a'}"
