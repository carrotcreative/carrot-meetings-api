rooms       = require './rooms'
meetings    = require './meetings'
express     = require 'express'

module.exports = app = express()

app.use rooms
app.use meetings
