Meeting = require '../models/meeting'
express = require 'express'

module.exports = app = express()

app.get '/meetings', (req, res) ->
  Meeting
    .collection()
    .fetch withRelated: ['room', 'invites', 'participants']
    .then (collection) -> res.json collection.toJSON()
    .catch (error) -> res.status(400).json(error)

app.get '/meetings/pending', (req, res) ->
  new Meeting()
    .where_is_pending()
    .fetchAll withRelated: ['room', 'invites', 'participants']
    .then (collection) -> res.json collection.toJSON()
    .catch (error) -> res.status(400).json(error)

app.get '/meetings/approved', (req, res) ->
  new Meeting()
    .where_is_approved()
    .fetchAll withRelated: ['room', 'invites', 'participants']
    .then (collection) -> res.json collection.toJSON()
    .catch (error) -> res.status(400).json(error)

app.get '/meetings/today', (req, res) ->
  new Meeting()
    .is_on_date new Date()
    .fetchAll withRelated: ['room', 'invites', 'participants']
    .then (collection) -> res.json collection.toJSON()
    .catch (error) -> res.status(400).json(error)

app.get '/meetings/on/:date', (req, res) ->
  new Meeting()
    .is_on_date req.param('date')
    .fetchAll withRelated: ['room', 'invites', 'participants']
    .then (collection) -> res.json collection.toJSON()
    .catch (error) -> res.status(400).json(error)

app.get '/meetings/between/:start_date/:end_date', (req, res) ->
  new Meeting()
    .between_dates req.param('start_date'), req.param('end_date')
    .fetchAll withRelated: ['room', 'invites', 'participants']
    .then (collection) -> res.json collection.toJSON()
    .catch (error) -> res.status(400).json(error)

app.get '/meetings/:id', (req, res) ->
  new Meeting id: req.param('id')
    .fetch withRelated: ['room', 'invites', 'participants']
    .then (model) -> res.json model.toJSON()
    .catch (error) -> res.status(400).json(error)

app.post '/meetings', (req, res) ->
  new Meeting req.body
    .save()
    .then (model) -> res.json model.toJSON()
    .catch (error) -> res.status(400).json(error)

app.patch '/meetings/:id', (req, res) ->
  new Meeting id: req.param('id')
    .fetch()
    .then (model) -> model.save(req.body)
    .then (model) -> res.json model.toJSON()
    .catch (error) -> res.status(400).json(error)

app.patch '/meetings/:id/approve', (req, res) ->
  new Meeting id: req.param('id')
    .fetch()
    .then (model) -> model.approve(req.body)
    .then (model) -> res.json model.toJSON()
    .catch (error) -> res.status(400).json(error)

app.delete '/meetings/:id', (req, res) ->
  new Meeting id: req.param('id')
    .fetch()
    .then (model) -> model.destroy()
    .then (model) -> res.json model.toJSON()
    .catch (error) -> res.status(400).json(error)
