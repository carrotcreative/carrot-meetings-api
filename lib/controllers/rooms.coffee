Room      = require '../models/room'
Meeting   = require '../models/meeting'
express   = require 'express'

module.exports = app = express()

app.get '/rooms', (req, res) ->
  Room
    .collection()
    .fetch()
    .then (collection) -> res.json collection.toJSON()
    .catch (error) -> res.status(400).json(error)

app.get '/rooms/:id', (req, res) ->
  new Room id: req.param('id')
    .fetch()
    .then (model) -> res.json model.toJSON()
    .catch (error) -> res.status(400).json(error)

app.get '/rooms/:id/meetings', (req, res) ->
  new Meeting()
    .where room_id: req.param('id')
    .fetch withRelated: ['invites', 'participants']
    .then (collection) -> res.json collection.toJSON()
    .catch (error) -> res.status(400).json(error)

app.get '/rooms/:id/meetings/today', (req, res) ->
  new Meeting()
    .is_on_date new Date()
    .where room_id: req.param('id')
    .fetchAll withRelated: ['invites', 'participants']
    .then (collection) -> res.json collection.toJSON()
    .catch (error) -> res.status(400).json(error)

app.get '/rooms/:id/meetings/pending', (req, res) ->
  new Meeting()
    .is_on_date new Date()
    .where_is_pending()
    .where room_id: req.param('id')
    .fetchAll withRelated: ['invites', 'participants']
    .then (collection) -> res.json collection.toJSON()
    .catch (error) -> res.status(400).json(error)

app.get '/rooms/:id/meetings/approved', (req, res) ->
  new Meeting()
    .is_on_date new Date()
    .where_is_approved()
    .where room_id: req.param('id')
    .fetchAll withRelated: ['invites', 'participants']
    .then (collection) -> res.json collection.toJSON()
    .catch (error) -> res.status(400).json(error)

app.get '/rooms/:id/meetings/on/:date', (req, res) ->
  new Meeting()
    .is_on_date req.param('date')
    .where room_id: req.param('id')
    .fetchAll withRelated: ['invites']
    .then (collection) -> res.json collection.toJSON()
    .catch (error) -> res.status(400).json(error)

app.get '/rooms/:id/meetings/between/:start_date/:end_date', (req, res) ->
  new Meeting()
    .between_dates req.param('start_date'), req.param('end_date')
    .where room_id: req.param('id')
    .fetchAll withRelated: ['invites']
    .then (collection) -> res.json collection.toJSON()
    .catch (error) -> res.status(400).json(error)

app.patch '/rooms/:id', (req, res) ->
  new Room id: req.param('id')
    .fetch()
    .then (model) -> model.save(req.body)
    .then (model) -> res.json model.toJSON()
    .catch (error) -> res.status(400).json(error)
