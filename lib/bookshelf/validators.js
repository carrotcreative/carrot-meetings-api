var Promise = require('bluebird')
  , path    = require('path');

var __ = {
  presence: require('./validators/presence')
  , operator: require('./validators/operator')
  , match: require('./validators/match')
};

module.exports = function (Bookshelf, validatorsPath) {

  var proto = Bookshelf.Model.prototype;

  var Model = Bookshelf.Model.extend({

    constructor: function () {
      this.validators = [];
      return proto.constructor.apply(this, arguments);
    },

    validate: function () {
      var args = Array.prototype.splice.call(arguments, 1)
        , validator = new Validator(this, arguments[0], args);

      this.validators.push(validator);
      return validator;
    },

    save: function () {
      var args = arguments;

      return this.triggerThen('validating', this)
        .bind(this)
        .then(function () {
          return Promise.map(this.validators, function (validator) {
            return validator.run();
          });
        })
        .then(function (validators) {
          var errors = []
            , validator;
          for (var key in validators) {
            validator = validators[key];
            if (validator.error) errors.push(validator.error);
          }
          if (errors.length) throw errors;
          return this.triggerThen('validated', this);
        })
        .then(function () {
          return proto.save.apply(this, arguments);
        });
    }

  });

  function Validator (model, name, args) {
    this.model = model;
    this.args = args;
    this.ifs = [];
    this._validator = _validator(name)(model);
  }

  Validator.prototype = {

    if: function (fn) {
      this.ifs.push(fn);
      return this;
    },

    msg: function (obj) {
      this._msg = obj;
      return this;
    },

    run: function () {
      var th = this;

      if (th.ifs.length) {
        var fn;
        for (var key in th.ifs) {
          fn = th.ifs[key];
          if (!!!fn(th.model)) return th;
        }
      }
      return Promise.method(function () {
        return th._validator.validate.apply(null, th.args);
      })().catch(function(error) {
        if (th._msg) {
          if (typeof th._msg === 'function') {
            error = th._msg(this.model).toString();
          } else {
            error = th._msg.toString();
          }
        }
        th.error = error;
      }).then(function () {
        return th;
      });
    }

  }

  function _validator(name) {
    if (!__[name] && validatorsPath) {
      __[name] = require(path.join(validatorsPath, name));
    }
    return __[name];
  }

  Bookshelf.Model = Model;

}
