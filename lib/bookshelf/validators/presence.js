module.exports = function (model) {

  return {

    validate: function (attribute) {
      var attr = model.get(attribute);
      if (!attr) throw 'The ' + attribute + ' is required';
    }

  }

}
