module.exports = function (model) {

  var __ = {
    email:              /^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,6}$/i
    , integer:          /^\-?[0-9]+$/
    , url:              /^((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/
    , natural:          /^[0-9]+$/i
    , naturalNonZero:   /^[1-9][0-9]*$/i
  }

  return {

    validate: function (attribute, type) {
      var attr = model.get(attribute);
      if (!__[type].test(attr)) throw 'The ' + attribute + ' must be a valid e-mail address';
    }

  }

}
