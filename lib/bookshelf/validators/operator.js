module.exports = function (model) {

  return {

    validate: function (lh, o, rh) {
      var b, t;
      lh = model.attributes[lh]
      rh = model.attributes[rh];
      switch (o) {
        case '>':
          t = 'greater than';
          b = lh > rh;
          break;
        case '<':
          t = 'less than';
          b = lh < rh;
          break;
        case '<=':
          t = 'less than or equal to';
          b = lh <= rh;
          break;
        case '>=':
          t = 'greater than or equal to';
          b = lh >= rh;
          break;
        case '=':
          t = 'equal to';
          b = lh == rh;
          break;
      }
      if (!b) throw lh + ' must be ' + t + ' ' + rh;
    }

  }

}
